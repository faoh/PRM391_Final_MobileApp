# API getting student data

## Quick start

```
npm install
node api.js
```

## Configuration
Edit config.js
### Port
By default, it's
```
8081
```
### Database name
```
mobile
```


## URL
```
getStudent?rollNumber=SE123456
getStudent?name=BaoNguyen
```