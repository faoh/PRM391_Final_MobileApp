package vn.edu.fpt.hoadx.is1103_finalproject.chatbox;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import vn.edu.fpt.hoadx.is1103_finalproject.chatbox.entity.Message;
import vn.edu.fpt.hoadx.is1103_finalproject.chatbox.entity.UserMessages;

public class ChatHistory {
    private SharedPreferences sp;
    private final String keySharedPreferences = "PsyDuck";
    private Gson gson;
    private int numOfMes = 0;
    UserMessages allMes;
    private final int mesPerScroll = 5;

    public ChatHistory(Context context, String userName) {
        this.sp = context.getSharedPreferences(keySharedPreferences, Context.MODE_PRIVATE);
        gson = new Gson();
        loadAllMessagesFromSharedPreferences(userName);//for allMes
    }

    private void commitMessages() {
        SharedPreferences.Editor editor = sp.edit();
        String json = gson.toJson(allMes);
        editor.putString(allMes.getUserName(), json);
        editor.commit();
    }

    protected void loadAllMessagesFromSharedPreferences(String userName) {
        String json = sp.getString(userName, "");
        allMes = gson.fromJson(json, UserMessages.class);
        if (allMes == null) {
            allMes = new UserMessages("", userName, new ArrayList<Message>());
        }
    }

    //return list of Messages, increase 6 mes per cast
    protected UserMessages getOldMessages() {
        if (allMes.getListMes().isEmpty()||allMes == null) {
            return null;
        }
        numOfMes = numOfMes + mesPerScroll;
        if (numOfMes > allMes.getListMes().size()) {
            return allMes;
        } else {
            List<Message> listMes = new ArrayList<>();
            int size = allMes.getListMes().size();
            for (int i = 0; i < numOfMes; i++) {
                listMes.add(allMes.getListMes().get(i));
            }
            return new UserMessages(allMes.getUserId(), allMes.getUserName(), listMes);
        }
    }


    protected void saveNewMessage(Message message) {
        allMes.getListMes().add(0, message);
        commitMessages();
    }


}
