package vn.edu.fpt.hoadx.is1103_finalproject.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

import vn.edu.fpt.hoadx.is1103_finalproject.R;

public class LoginActivity extends AppCompatActivity {

    private Button mConnectButton;
    private TextInputEditText mUserIdEditText, mUserNicknameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mConnectButton = findViewById(R.id.button_login);
        mUserNicknameEditText = findViewById(R.id.edit_text_login_user_nickname);
        mUserNicknameEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER){
                    loginning();
                }
                return false;
            }
        });
        mConnectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginning();
            }
        });
    }

    private void loginning(){
        String userN = mUserNicknameEditText.getText().toString();
        // Remove all spaces from userID
        userN = userN.replaceAll("\\s", "");
        userN = userN.trim();
        Intent intent = new Intent(LoginActivity.this, ChatActivity.class);
        intent.putExtra("userN", userN);
        startActivity(intent);
        finish();
    }


}
