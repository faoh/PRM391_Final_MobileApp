package vn.edu.fpt.hoadx.is1103_finalproject.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import vn.edu.fpt.hoadx.is1103_finalproject.R;
import vn.edu.fpt.hoadx.is1103_finalproject.chatbox.entity.Message;
import vn.edu.fpt.hoadx.is1103_finalproject.utils.Utils;
import vn.edu.fpt.hoadx.is1103_finalproject.chatbox.ChatAdapter;

public class ChatActivity extends AppCompatActivity {

    private ChatAdapter mChatAdapter;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private Button mSendButton;
    private EditText mMessageEditText;
    private String BOT = "PsyDuck";
    private String urlProfileImage = "https://sv1.uphinhnhanh.com/images/2018/07/17/psyduckBot2.jpg";
    String intent;
    String currentUserName;
    boolean isGreeting;
    boolean isQuestion;
    private String userN;

    public String getUserN() {
        return userN;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        userN = getIntent().getStringExtra("userN");//Get userN from Login
        mSendButton = findViewById(R.id.button_chat_send);
        mMessageEditText = findViewById(R.id.edittext_chat);

        mRecyclerView = findViewById(R.id.reycler_chat);
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setReverseLayout(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mChatAdapter = new ChatAdapter(this);
        mRecyclerView.setAdapter(mChatAdapter);

        mMessageEditText.setText("");
        mMessageEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    pushMsg();
                }
                return false;
            }
        });
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushMsg();
            }
        });
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (mLayoutManager.findLastVisibleItemPosition() == mChatAdapter.getItemCount() - 1) {
                    Toast.makeText(getApplication(), "Scrolled", Toast.LENGTH_SHORT).show();
                    mChatAdapter.loadPreviousMessages();
                }
            }
        });

        intent = "";
        currentUserName = "";
    }

    private void pushMsg() {
        String msg = mMessageEditText.getText().toString();
        mChatAdapter.appendMessage(userN, msg);
        mMessageEditText.setText("");
        extractText(msg);
    }

    private void extractText(final String text) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    String witEndpoint = "https://api.wit.ai/message?v=20180716&verbose=true&q=" + text;
                    String jsonString = null;
                    jsonString = Utils.getAPI(witEndpoint);

                    if (jsonString.isEmpty()) return;

                    Log.v("DumpData", jsonString);

                    JSONObject root = new JSONObject(jsonString);
                    JSONObject entities = root.getJSONObject("entities");

                    String response = "";
                    /**
                     * Greetings
                     */
                    if (Utils.extractEntity(entities, "greeting", "true", 0.8) != "") {
                        // greetings
                        if (!isGreeting) {
                            mChatAdapter.appendMessage(BOT, "Hi there\nI am PsyDuck Bot. How can I help you?\n");
                        } else {
                            mChatAdapter.appendMessage(BOT, "Hi there\n");
                        }
                        isGreeting = true;
                        return;
                    }
                    /**
                     * Goodbye
                     */
                    if (Utils.extractEntity(entities, "bye", "true", 0.8) != "") {
                        // greetings
                        mChatAdapter.appendMessage(BOT, "Goodbye, see ya.\n");
                        isGreeting = false;
                        return;
                    }
                    /**
                     * Get Student Info
                     */
                    if (Utils.extractEntity(entities, "intent", "get_student_info", 0.8) != "") {
                        // user want to get student information
                        intent = "get_student_info";
                        if ((response = Utils.extractEntity(entities, "contact", "", 0.8)) != "") {
                            mChatAdapter.appendMessage(BOT, "You want to get information of student " + response.substring(0, 1).toUpperCase() + response.substring(1) + ", don't you?\n");
                            isQuestion = true;
                        } else if ((response = Utils.extractEntity(entities, "roll_number", "true", 0.8)) != "") {
                            mChatAdapter.appendMessage(BOT, "You want to get information of student has roll number" + response + ", don't you ?\n");
                            isQuestion = true;
                        } else if (!currentUserName.isEmpty()) {
                            mChatAdapter.appendMessage(BOT, "You want to get information of student " + currentUserName + ", don't you?\n");
                            isQuestion = true;
                        } else {
                            mChatAdapter.appendMessage(BOT, "What student's name do you want to see?\n");
                        }
                        return;
                    }
                    /**
                     * User enter his name
                     */
                    if (Utils.extractEntity(entities, "intent", "current_user_name", 0.8) != "") {
                        // user want to get student information
                        intent = "current_user_name";
                        if ((response = Utils.extractEntity(entities, "contact", "", 0.8)) != "") {
                            currentUserName = response.substring(0, 1).toUpperCase() + response.substring(1);
                            mChatAdapter.appendMessage(BOT, "Hi " + currentUserName + ". How can I help you?\n");
                        }
                        return;
                    }
                    /**
                     * User enter a name
                     */
                    if ((response = Utils.extractEntity(entities, "contact", "", 0.8)) != "") {
                        if (intent.equals("get_student_info")) {
                            mChatAdapter.appendMessage(BOT, "You want to get information of student " + response.substring(0, 1).toUpperCase() + response.substring(1) + ", don't you ?\n");
                            isQuestion = true;
                        } else {
                            mChatAdapter.appendMessage(BOT, "Oops!!! I cannot recognize what do you want. Sorry about that.\n");
                        }
                        return;
                    }
                    /**
                     * User enter a roll number
                     */
                    if ((response = Utils.extractEntity(entities, "roll_number", "true", 0.8)) != "") {
                        if (intent.equals("get_student_info")) {
                            mChatAdapter.appendMessage(BOT, "You want to get information of student has roll number" + response + ", don't you ?\n");
                            isQuestion = true;
                        } else if (intent.equals("current_user_name")) {
                            mChatAdapter.appendMessage(BOT, "Hi " + response + ". How can I help you?\n");
                        } else {
                            mChatAdapter.appendMessage(BOT, "Oops!!! I cannot recognize what do you want. Sorry about that.\n");
                        }
                        return;
                    }

                    /**
                     * User answer the question
                     */
                    if (isQuestion) {
                        if (Utils.extractEntity(entities, "intent", "yes", 0.8) != "") {
                            if (intent.equals("get_student_info")) {
                                mChatAdapter.appendMessage(BOT, "Wait a minute, I am searching student information...\n");
                            }
                            isQuestion = false;
                            return;
                        }
                        if (Utils.extractEntity(entities, "intent", "no", 0.8) != "") {
                            mChatAdapter.appendMessage(BOT, "Thank you. Do you have any question?\n");
                            isQuestion = false;
                            return;
                        }
                    }
                    /**
                     * Can't understand
                     */
                    mChatAdapter.appendMessage(BOT, "I'm sorry. What do you want?\n");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        });
    }


}
