package vn.edu.fpt.hoadx.is1103_finalproject.chatbox.messageHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import vn.edu.fpt.hoadx.is1103_finalproject.chatbox.entity.Message;
import vn.edu.fpt.hoadx.is1103_finalproject.R;
import vn.edu.fpt.hoadx.is1103_finalproject.utils.ImageUtils;
import vn.edu.fpt.hoadx.is1103_finalproject.utils.TimeUtils;
import vn.edu.fpt.hoadx.is1103_finalproject.utils.Utils;

// Messages sent by me do not display a profile image or nickname.
public class SentMessageHolder extends RecyclerView.ViewHolder {
    TextView messageText, timeText;

    public SentMessageHolder(View itemView) {
        super(itemView);

        messageText = itemView.findViewById(R.id.text_message_body);
        timeText = itemView.findViewById(R.id.text_message_time);
    }

    public void bind(Message message) {
        messageText.setText(message.getMsg());

        // Format the stored timestamp into a readable String using method.
        timeText.setText(TimeUtils.formatTime(message.getCreatedAt()));
    }
}
