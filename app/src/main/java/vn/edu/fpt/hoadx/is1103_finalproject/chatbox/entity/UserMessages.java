package vn.edu.fpt.hoadx.is1103_finalproject.chatbox.entity;

import java.util.List;

public class UserMessages {
    private String userId;
    private String userName;
    List<Message> listMes;

    public UserMessages(String userId, String userName, List<Message> listMes) {
        this.userId = userId;
        this.userName = userName;
        this.listMes = listMes;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<Message> getListMes() {
        return listMes;
    }

    public void setListMes(List<Message> listMes) {
        this.listMes = listMes;
    }

    public UserMessages() {

    }
}
