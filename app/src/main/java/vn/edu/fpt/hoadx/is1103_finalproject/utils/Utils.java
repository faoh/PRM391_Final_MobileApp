package vn.edu.fpt.hoadx.is1103_finalproject.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

public class Utils {
    public static String extractEntity(JSONObject entities, String keyword, String value, double confidence) throws JSONException {
        String response = "";
        if (!entities.isNull(keyword)) {
            // Get by student name
            JSONArray result = entities.getJSONArray(keyword);
            for (int i = 0; i < result.length(); i++) {
                JSONObject entity = result.getJSONObject(i);
                if (value == "") {
                    if (entity.getDouble("confidence") > confidence) {
                        confidence = entity.getDouble("confidence");
                        response = entity.getString("value");
                    }
                } else if (entity.getString("value").equals(value)) {
                    if (entity.getDouble("confidence") > confidence) {
                        confidence = entity.getDouble("confidence");
                        if (!entity.isNull("_body"))
                            response = entity.getString("_body");
                        else if (!entity.isNull("_entity"))
                            response = entity.getString("_entity");
                    }
                }
            }
        }
        return response;
    }

    public static String getAPI(String url) throws IOException {
        URL endpoint = new URL(url);
        HttpsURLConnection conn = (HttpsURLConnection) endpoint.openConnection();
        conn.setRequestProperty("Authorization", "Bearer QT7BQQAVI4UBAN262IDEYSQVHBVD44KJ");

        if (conn.getResponseCode() == 200) {
            InputStream is = new BufferedInputStream(conn.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();

            String line;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append('\n');
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            String jsonString = sb.toString();
            conn.disconnect();
            return jsonString;
        }
        return "";
    }

    private Utils() {

    }
}
